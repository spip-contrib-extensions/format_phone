<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'format_phone_description' => 'Intègre la librairie phoneformat.js (https://github.com/albeebe/phoneformat.js) pour formatter joliment les n° de téléphone.',
	'format_phone_nom' => 'Formatteur de n° de téléphone',
	'format_phone_slogan' => 'Des n° de téléphone joliment formattés !',
);