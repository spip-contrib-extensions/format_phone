<?php
/**
 * Options au chargement du plugin Formatteur de n° de téléphone
 *
 * @plugin     Formatteur de n° de téléphone
 * @copyright  2016
 * @author     Michel @ Vertige ASBL
 * @licence    GNU/GPL
 * @package    SPIP\Format_phone\Options
 */

if (! defined('FORMAT_PHONE_PAYS')) {
	define('FORMAT_PHONE_PAYS', 'BE');
}
